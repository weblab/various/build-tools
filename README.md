# WebLab Build Tools

This project is a very simple Maven project containing:
* the custom checkstyle configuration to be used by Maven Checkstyle Plugin,
* some Eclipse configuration files (code template, formatter and cleanup) that can be imported.


## Checkstyle

In order to use the custom checkstyle rules inside your WebLab based project, you have two options:
1. use as parent one of our "parent" pom.xml depending on the type of project you are realising; since we have already configured our parents to setup checkstyle for subprojects. (TODO add link to projects here)
2. setup the checktype plugin like it is done in our poms.

To configure it by yourself, you will have to:
- Define version of the maven checkstyle plugin in the plugin management section:

	```xml
	<build>
		[...]
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-checkstyle-plugin</artifactId>
					<version>2.17</version>
				</plugin>
				[...]
			</plugins>
		</pluginManagement>
		[...]
	</build>
	```

- Configure the plugin in the plugins section:

	```xml
	<build>
		[...]
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<configuration>
					<configLocation>checkstyle/weblab_checks.xml</configLocation>
					<encoding>${project.build.sourceEncoding}</encoding>
					<includeTestSourceDirectory>false</includeTestSourceDirectory>
				</configuration>
			</plugin>
			[...]
		</plugins>
		[...]
	</build>
	```

- Configure the report in the reporting section
	```xml
	<reporting>
		[...]
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<reportSets>
					<reportSet>
						<reports>
							<report>checkstyle</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			[...]
		</plugins>
		[...]
	</reporting>
	```

## Eclipse

The main things to be configured in your eclipse could be imported using the files provided in the src/main/resources/eclipse subfolder of this project.
However some very important parameters could not:
- Encoding: UTF-8. (This has to be set for workspace and for every single file types [xml, css, html...]).
- Line Ending: Linux (\n)
- Line width: 200


# Build status
[![build status](https://gitlab.ow2.org/weblab/various/build-tools/badges/master/build.svg)](https://gitlab.ow2.org/weblab/various/build-tools/commits/master)
